import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'healthshare-prototype';
  openModal = false;

  handleOpenModal(event) {
    this.openModal = true;
  }
  handleCloseModal(event) {
    this.openModal = false;
  }
}
