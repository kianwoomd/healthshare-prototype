import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HxUiModule } from '@hxui/angular';
import { MockHelixModule } from './mock-helix/mock-helix.module';
import { PractitionerSearchModule } from './practitioner-search/practitioner-search.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HxUiModule.forRoot(),
    MockHelixModule,
    PractitionerSearchModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

