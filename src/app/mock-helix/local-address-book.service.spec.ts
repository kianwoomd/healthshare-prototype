import { TestBed } from '@angular/core/testing';

import { LocalAddressBookService } from './local-address-book.service';

describe('LocalAddressBookService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalAddressBookService = TestBed.get(LocalAddressBookService);
    expect(service).toBeTruthy();
  });
});
