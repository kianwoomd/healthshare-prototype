import { Injectable } from '@angular/core';
import {empty, Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {catchError, delay} from 'rxjs/operators';
import {apiToken, apiToken_, practitionersUrl} from '../practitioner-search/services/healthshare.service';
import {PractitionerSearchResult} from '../practitioner-search/models/practitioner.interface';


export interface LocalPractitioner {  // only including fields that are displayed
  FormattedName: string;
  Phone: string;
  PhysicalAddress: { Suburb: string, State: string };
  Speciality: { Description: string };
}

export const localPractitionersUrl = 'https://healthshare-referrals-prototype.netlify.com:3000/practiceAddressBook';

@Injectable({
  providedIn: 'root'
})
export class LocalAddressBookService {

  constructor(
    private http: HttpClient
  ) { }

  public getLocalPractitioners(): Observable<LocalPractitioner[]> {
      return this.http.get(localPractitionersUrl)
        .pipe(
          catchError(this.handleError('getLocalPractitioners', null))
        );
  }

  public searchLocalPractitioners(searchString: string): Observable<LocalPractitioner[]> {
    const params = new HttpParams({ fromObject:
        { q: searchString,
          _limit: '10',
        }
    } );
    const headers = new HttpHeaders({}) ;
    const options = { headers , params };

    return this.http.get<LocalPractitioner>(localPractitionersUrl, options)
      .pipe(
        catchError(this.handleError('searchLocalPractitioners', null))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`HealthshareService: ${message}`);
  }
}
