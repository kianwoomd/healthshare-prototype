import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MockHelixComponent } from './mock-helix.component';

describe('MockHelixComponent', () => {
  let component: MockHelixComponent;
  let fixture: ComponentFixture<MockHelixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MockHelixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockHelixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
