import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { searchSteps } from '../practitioner-search/state';
import { Observable, Subject } from 'rxjs';
import { LocalAddressBookService, LocalPractitioner } from './local-address-book.service';
import {debounceTime, distinctUntilChanged, filter, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-mock-helix',
  templateUrl: './mock-helix.component.html',
  styleUrls: ['./mock-helix.component.scss']
})
export class MockHelixComponent implements OnInit {

  @Output()
  openModal: EventEmitter<boolean> = new EventEmitter();

  private searchText$ = new Subject<string>();

  localPractitioners$: Observable<LocalPractitioner[]>;

  constructor(
    private localAddressBookService: LocalAddressBookService
  ) { }

  ngOnInit() {
    // find practitioners that whose name contains the search string
    this.localPractitioners$ = this.searchText$.pipe(
      filter(searchString => searchString.length > 0),
      debounceTime(500),
      distinctUntilChanged(),
      switchMap((searchString) => {
          console.log('localPractitioners$ searchString ' + searchString);
          return this.localAddressBookService.searchLocalPractitioners(searchString);
        }
      )
    );
  }

  openPractitionerSearchModal = () => {
    // outputting an event to open the modal (non standard way .. for standard approach see comment code below and http://angular.hxui.io/#/modals )
    this.openModal.emit(true);
    // this.modalService.create<PractitionerSearchComponent>(PractitionerSearchComponent, {
    //   onSuccess: (data) => {
    //     alert(data);
    //   }
    // });
  }

  search(searchText: string) {
    console.log('search: ' + searchText);
    this.searchText$.next(searchText);
  }
}
