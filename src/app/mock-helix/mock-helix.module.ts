import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MockHelixComponent } from './mock-helix.component';
import {LoadingComponent} from '../practitioner-search/components/loading/loading.component';

@NgModule({
  declarations: [
    MockHelixComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    MockHelixComponent
  ]
})
export class MockHelixModule {}
