import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalActionsComponent } from './additional-actions.component';

describe('AdditionalActionsComponent', () => {
  let component: AdditionalActionsComponent;
  let fixture: ComponentFixture<AdditionalActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
