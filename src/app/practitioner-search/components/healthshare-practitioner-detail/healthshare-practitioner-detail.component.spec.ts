import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthsharePractitionerDetailComponent } from './healthshare-practitioner-detail.component';

describe('HealthsharePractitionerDetailComponent', () => {
  let component: HealthsharePractitionerDetailComponent;
  let fixture: ComponentFixture<HealthsharePractitionerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthsharePractitionerDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthsharePractitionerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
