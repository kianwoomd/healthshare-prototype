import { Component, Input, OnInit } from '@angular/core';
import { HealthshareService } from '../../services/healthshare.service';
import { Store } from '../../store';
import { PractitionerDetail } from '../../models/practitioner.interface';
import { Observable } from 'rxjs';
import { searchSteps } from '../../state';

@Component({
  selector: 'app-healthshare-practitioner-detail',
  templateUrl: './healthshare-practitioner-detail.component.html',
  styleUrls: ['./healthshare-practitioner-detail.component.scss']
})
export class HealthsharePractitionerDetailComponent implements OnInit {

  @Input()
  practitionerId;

  practitioner$: Observable<PractitionerDetail>;
  searchStepPrevious$: Observable<searchSteps>;

  constructor(
    private healthshareService: HealthshareService,
    private store: Store
  ) { }

  ngOnInit() {
    this.practitioner$ = this.healthshareService.getPractitioner(this.practitionerId);
    this.searchStepPrevious$ = this.store.select('searchStepPrevious');
  }

  getSpecialtyNames( specialtyIds: number[]): string {
    return this.healthshareService.getSpecialtyNames(specialtyIds).join(', ');
  }

  goBack(step: searchSteps) {
    this.store.set('searchStep', step);
  }

}
