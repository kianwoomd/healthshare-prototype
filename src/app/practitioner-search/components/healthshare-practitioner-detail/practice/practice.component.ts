import { Component, Input, OnInit } from '@angular/core';
import { Practice } from '../../../models/practice.interface';

@Component({
  selector: 'app-practice',
  templateUrl: './practice.component.html',
  styleUrls: ['./practice.component.scss']
})
export class PracticeComponent implements OnInit {

  @Input()
  practices: Practice[];

  constructor() { }

  ngOnInit() {
  }

}
