import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthshareResultsPractitionersComponent } from './healthshare-results-practitioners.component';

describe('HealthshareResultsPractitionersComponent', () => {
  let component: HealthshareResultsPractitionersComponent;
  let fixture: ComponentFixture<HealthshareResultsPractitionersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthshareResultsPractitionersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthshareResultsPractitionersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
