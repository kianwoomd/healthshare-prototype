import { Component, Input, OnInit } from '@angular/core';
import { Practitioner, PractitionerSearchResult } from '../../../models/practitioner.interface';
import { HealthshareService } from '../../../services/healthshare.service';
import { Store } from '../../../store';
import { searchSteps } from '../../../state';
import { SearchHighlightPipe } from '../../../pipes/search-highlight.pipe';

@Component({
  selector: 'app-healthshare-results-practitioners',
  templateUrl: './healthshare-results-practitioners.component.html',
  styleUrls: ['./healthshare-results-practitioners.component.scss'],
  providers: [ SearchHighlightPipe ]
})
export class HealthshareResultsPractitionersComponent implements OnInit {

  @Input()
  practitionersSearchResult: PractitionerSearchResult;

  @Input()
  searchString: string;

  constructor(
    private healthshareService: HealthshareService,
    private store: Store,
    private searchHighlightPipe: SearchHighlightPipe
  ) { }

  ngOnInit() {
  }

  getSpecialtyNames( specialtyIds: number[]): string {
    return this.healthshareService.getSpecialtyNames(specialtyIds).join(', ');
  }

  choosePractitioner( practitionerId: string) {
    this.store.set('selectedPractitionerId', practitionerId);
    this.store.set('searchStep', searchSteps.PractitionerDetail);
    this.store.set('searchStepPrevious', searchSteps.Matching);
  }

  formatPractitionerName( practitioner: Practitioner) {
    const title = practitioner.title ? practitioner.title + ' ' : '';
    const name  = this.searchHighlightPipe.transform(practitioner.first_name + ' ' + practitioner.last_name, this.searchString);
    return title + name;
  }
}
