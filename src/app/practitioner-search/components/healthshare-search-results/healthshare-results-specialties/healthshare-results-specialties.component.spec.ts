import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthshareResultsSpecialtiesComponent } from './healthshare-results-specialties.component';

describe('HealthshareResultsSpecialtiesComponent', () => {
  let component: HealthshareResultsSpecialtiesComponent;
  let fixture: ComponentFixture<HealthshareResultsSpecialtiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthshareResultsSpecialtiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthshareResultsSpecialtiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
