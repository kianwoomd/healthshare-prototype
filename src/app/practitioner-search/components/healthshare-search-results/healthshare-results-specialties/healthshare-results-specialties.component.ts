import { Component, Input, OnInit } from '@angular/core';
import { Specialty } from '../../../models/specialty.interface';
import { Store } from '../../../store';
import { searchSteps } from '../../../state';

@Component({
  selector: 'app-healthshare-results-specialties',
  templateUrl: './healthshare-results-specialties.component.html',
  styleUrls: ['./healthshare-results-specialties.component.scss']
})
export class HealthshareResultsSpecialtiesComponent implements OnInit {

  @Input()
  specialties: Specialty[];

  @Input()
  searchString: string;

  chooseSpecialty(id: number) {
    this.store.set('selectedSpecialtyId', id);
    this.store.set('searchStep', searchSteps.PractitionersBySpecialtyId);
  }

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
  }

}
