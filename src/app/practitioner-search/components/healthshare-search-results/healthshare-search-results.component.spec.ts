import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthshareSearchResultsComponent } from './healthshare-search-results.component';

describe('HealthshareSearchResultsComponent', () => {
  let component: HealthshareSearchResultsComponent;
  let fixture: ComponentFixture<HealthshareSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthshareSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthshareSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
