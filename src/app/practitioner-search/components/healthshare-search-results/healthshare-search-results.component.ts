import { Component, Input, OnInit } from '@angular/core';
import { Practitioner, PractitionerSearchResult } from '../../models/practitioner.interface';
import { Specialty } from '../../models/specialty.interface';
import { Store } from '../../store';

@Component({
  selector: 'app-healthshare-search-results',
  templateUrl: './healthshare-search-results.component.html',
  styleUrls: ['./healthshare-search-results.component.scss']
})
export class HealthshareSearchResultsComponent implements OnInit {
  @Input()
  specialties: Specialty[];

  // @Input()
  // practitioners: Practitioner[];

  @Input()
  practitionersSearchResult: PractitionerSearchResult;

  @Input()
  searchString: string;

  constructor( private store: Store ) { }

  ngOnInit() {
  }

}
