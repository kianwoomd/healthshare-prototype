import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthshareSignatureComponent } from './healthshare-signature.component';

describe('HealthshareSignatureComponent', () => {
  let component: HealthshareSignatureComponent;
  let fixture: ComponentFixture<HealthshareSignatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthshareSignatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthshareSignatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
