import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthshareSpecialtyResultsComponent } from './healthshare-specialty-results.component';

describe('HealthshareSpecialtyResultsComponent', () => {
  let component: HealthshareSpecialtyResultsComponent;
  let fixture: ComponentFixture<HealthshareSpecialtyResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthshareSpecialtyResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthshareSpecialtyResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
