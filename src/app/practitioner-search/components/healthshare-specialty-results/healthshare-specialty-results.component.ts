import { Component, Input, OnInit } from '@angular/core';
import { HealthshareService } from '../../services/healthshare.service';
import { Store } from '../../store';
import { Practitioner, PractitionerSearchResult } from '../../models/practitioner.interface';
import { Observable } from 'rxjs';
import { searchSteps } from '../../state';

@Component({
  selector: 'app-healthshare-specialty-results',
  templateUrl: './healthshare-specialty-results.component.html',
  styleUrls: ['./healthshare-specialty-results.component.scss']
})
export class HealthshareSpecialtyResultsComponent implements OnInit {

  @Input()
  specialtyId;

  locality = '269'; // Haymarket

  practitionerSearchResults$: Observable<PractitionerSearchResult>;

  specialtyName;

  constructor(
    private healthshareService: HealthshareService,
    private store: Store
  ) {}

  ngOnInit() {
    this.practitionerSearchResults$ = this.healthshareService.searchPractitionersBySpecialtyAndLocation(this.specialtyId, this.locality);
    this.specialtyName = this.healthshareService.specialtiesAll
      .filter( specialty =>
        specialty.id === this.specialtyId
      )
      .map(specialty => specialty.name );

  }

  getSpecialtyNames( specialtyIds: number[]): string {
    return this.healthshareService.getSpecialtyNames(specialtyIds).join(', ');
  }

  choosePractitioner( practitionerId: number) {
    this.store.set('selectedPractitionerId', practitionerId);
    this.store.set('searchStep', searchSteps.PractitionerDetail);
    this.store.set('searchStepPrevious', searchSteps.PractitionersBySpecialtyId);
  }

  goBack() {
    this.store.set('searchStep', searchSteps.Matching);
  }
}
