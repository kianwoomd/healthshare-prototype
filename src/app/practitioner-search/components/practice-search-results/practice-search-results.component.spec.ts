import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticeSearchResultsComponent } from './practice-search-results.component';

describe('PracticeSearchResultsComponent', () => {
  let component: PracticeSearchResultsComponent;
  let fixture: ComponentFixture<PracticeSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PracticeSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticeSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
