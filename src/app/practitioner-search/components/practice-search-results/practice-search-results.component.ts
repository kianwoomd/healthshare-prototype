import { Component, OnInit } from '@angular/core';


// This is a static HTML Mockup. Angular cosw is for prototype purposes only

@Component({
  selector: 'app-practice-search-results',
  templateUrl: './practice-search-results.component.html',
  styleUrls: ['./practice-search-results.component.scss']
})
export class PracticeSearchResultsComponent implements OnInit {


  showLance = false;
  constructor() { }

  ngOnInit() {
  }

  // prototype functions
  toggleLance() {
    this.showLance = ! this.showLance;
  }

  selectName() {
    alert('name selected');
    event.stopPropagation();
  }
}
