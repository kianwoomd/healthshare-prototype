import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { HealthshareService } from '../services/healthshare.service';
import { Practitioner, PractitionerSearchResult } from '../models/practitioner.interface';
import { Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';
import { Specialty } from '../models/specialty.interface';
import { Store } from '../store';
import { searchSteps } from '../state';

@Component({
  selector: 'app-practitioner-search',
  templateUrl: './practitioner-search.component.html',
  styleUrls: ['./practitioner-search.component.scss']
})
export class PractitionerSearchComponent implements OnInit {

  @Output()
  closeModal: EventEmitter<boolean> = new EventEmitter();

  withRefresh = false;
  practitionersSearchResult$: Observable<PractitionerSearchResult>;

  // specialtiesAll: Specialty[];
  specialtiesMatching$: Observable<Specialty[]>;

  private searchText$ = new Subject<string>();

  searchStepsEnum = searchSteps;
  searchStep$: Observable<number>;
  selectedSpecialtyId$: Observable<number>;
  selectedPractitionerId$: Observable<number>;
  selectedTabIndex$: Observable<number>;

  @Input()
  isModalActive = false;

  search(searchText: string) {
    if (searchText.length > 0 ) {
      this.store.set('searchStep', searchSteps.Matching);
    } else {
      this.store.set('searchStep', searchSteps.Suggested);
    }
    this.searchText$.next(searchText);
  }

  ngOnInit() {

    // get specialities - moved to service
    // this.healthshareService
    //   .getSpecialties()
    //   .subscribe((data: Specialty[]) => this.specialtiesAll = data);

    // NOTE: the pipe links operators together.
    // A set of operators applied to an observable is a recipe—that is,
    // a set of instructions for producing the values you’re interested in.
    // By itself, the recipe doesn’t do anything. You need to call subscribe() to produce a result through the recipe.
    // In this case, we don't call subcribe, but subscription occurs in the template, via the async keyword

    // find practitioners that whose name contains the search string
    this.practitionersSearchResult$ = this.searchText$.pipe(
      // filter(searchString => searchString.length > 1),
      debounceTime(500),
      distinctUntilChanged(),
      switchMap((searchString) => {
          console.log('practitionersMatchingByName$ searchString ' + searchString);
          return this.healthshareService.searchPractitionersByName(searchString, this.withRefresh);
        }
      )
    );

    // find specialities who name contains the search string
    this.specialtiesMatching$ = this.searchText$.pipe(
      // filter(searchString => searchString.length > 1),
      debounceTime(500),
      distinctUntilChanged(),
      switchMap((searchString) => {
        console.log('specialtiesMatching$ searchString ' + searchString);
        return this.healthshareService.searchSpecialtiesByName(searchString);
      }
      )
    );

    this.searchStep$ = this.store.select('searchStep');
    this.selectedSpecialtyId$ = this.store.select('selectedSpecialtyId');
    this.selectedPractitionerId$ = this.store.select('selectedPractitionerId');
    this.selectedTabIndex$ = this.store.select('selectedTabIndex');

    // set focus
  }

  constructor(
    private healthshareService: HealthshareService,
    private store: Store
  ) {}

  onClose() {
    this.closeModal.emit(true);
  }

  setTab(tab: number ) {
    this.store.set('selectedTabIndex', tab);
  }

}
