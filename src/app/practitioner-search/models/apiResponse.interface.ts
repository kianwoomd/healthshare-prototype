export interface  ApiResponse {
  count: number;
  next: string;
  prev: string | null;
  results: any[];
}
