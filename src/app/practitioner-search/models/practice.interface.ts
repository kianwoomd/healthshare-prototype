export interface StartEnd {
  start: string;
  end: string;
}
export interface PracticeTradingHours {
  mon: StartEnd;
  tue: StartEnd;
  wed: StartEnd;
  thu: StartEnd;
  fri: StartEnd;
  sat: StartEnd;
  sun: StartEnd;
}
export interface Practice {
  id: number;
  name: string;
  street1: string;
  street2: string;
  suburb: string;
  post_code: string;
  state: string;
  fax: string;
  phones: string[];
  website: string;
  practice_management_system: any;
  is_booking_enabled: boolean;
  services: string[];
  hours: PracticeTradingHours;
}
