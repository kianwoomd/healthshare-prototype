import { ApiResponse } from './apiResponse.interface';
import { Practice } from './practice.interface';

export interface Interest {
  id: number;
  name: string;
  specialty: number;
}

export interface Practitioner {
  id: string;
  title: string | null;
  first_name: string;
  last_name: string;
  location: string;
  avatar: string;
  specialties: number[];
  profile_url: string;
  interests: any[];
  interests_description: string;
  qualifications_title: string;
}

export interface PractitionerDetail  extends Practitioner {
  name: string;
  gender: string;
  awards: string;
  bio: string;
  booking_url: string;
  registration_numbers: string;
  hospitals: number[];
  interest: Interest[];
  languages: string[];
  practices: Practice[];
  qualifications: string;
  profile_url: string;
  consultation_forms: any;
  referral_template_name: string;
  referrals_tagline: string;
  interests_description_html: string;
}

export interface PractitionerSearchResult extends ApiResponse {
  results: Practitioner[];
}
