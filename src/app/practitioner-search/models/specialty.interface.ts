import {ApiResponse} from './apiResponse.interface';

export interface Specialty {
  id: number;
  name: string;
  plural_name: string;
  professional_name: string;
  plural_professional_name: string;
}

export interface SpecialtySearchResult extends ApiResponse {
  results: Specialty[];
}
