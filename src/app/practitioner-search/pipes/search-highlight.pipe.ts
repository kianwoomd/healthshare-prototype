import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchHighlight'
})
export class SearchHighlightPipe implements PipeTransform {

  transform(value: string, highlightString: string): any {
    function replacer(match) {
      return `<span class="is-text-weight-bolder">${match}</span>`;
    }
    const regex = new RegExp(highlightString, 'gi');
    return value.replace(regex, replacer);
  }

}
