import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PractitionerSearchComponent } from './containers/practitioner-search.component';
import { HealthshareResultsPractitionersComponent } from './components/healthshare-search-results/healthshare-results-practitioners/healthshare-results-practitioners.component';
import { HealthshareResultsSpecialtiesComponent } from './components/healthshare-search-results/healthshare-results-specialties/healthshare-results-specialties.component';
import { HealthshareService } from './services/healthshare.service';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { SearchSuggestionsComponent } from './components/search-suggestions/search-suggestions.component';
import { HealthshareSearchResultsComponent } from './components/healthshare-search-results/healthshare-search-results.component';
import { HealthsharePractitionerDetailComponent } from './components/healthshare-practitioner-detail/healthshare-practitioner-detail.component';
import { HealthshareSpecialtyResultsComponent } from './components/healthshare-specialty-results/healthshare-specialty-results.component';
import { Store } from './store';
import { PracticeComponent } from './components/healthshare-practitioner-detail/practice/practice.component';
import { LoadingComponent } from './components/loading/loading.component';
import { SearchHighlightPipe } from './pipes/search-highlight.pipe';
import { HxUiModule } from '@hxui/angular';
import { PracticeSearchResultsComponent } from './components/practice-search-results/practice-search-results.component';
import { HealthshareSignatureComponent } from './components/healthshare-signature/healthshare-signature.component';
import { AdditionalActionsComponent } from './components/additional-actions/additional-actions.component';

@NgModule({
  declarations: [
    PractitionerSearchComponent,
    HealthshareResultsSpecialtiesComponent,
    HealthshareResultsPractitionersComponent,
    SearchSuggestionsComponent,
    HealthshareSearchResultsComponent,
    HealthsharePractitionerDetailComponent,
    HealthshareSpecialtyResultsComponent,
    PracticeComponent,
    LoadingComponent,
    SearchHighlightPipe,
    PracticeSearchResultsComponent,
    HealthshareSignatureComponent,
    AdditionalActionsComponent
  ],
  providers: [
    HealthshareService,
    Store
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    HttpClientJsonpModule,
    HxUiModule
  ],
  exports: [
    PractitionerSearchComponent,
    LoadingComponent
  ]
})
export class PractitionerSearchModule {}
