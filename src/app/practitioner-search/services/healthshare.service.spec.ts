import { TestBed } from '@angular/core/testing';

import { HealthshareService } from './healthshare.service';

describe('HealthshareService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HealthshareService = TestBed.get(HealthshareService);
    expect(service).toBeTruthy();
  });
});
