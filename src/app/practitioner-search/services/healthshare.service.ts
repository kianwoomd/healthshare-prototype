import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Practitioner, PractitionerDetail, PractitionerSearchResult } from '../models/practitioner.interface';
import { Specialty } from '../models/specialty.interface';

export const apiToken = '570fad18b6eeed9fc99e82f0ef6e59a360ea97f7';
export const apiToken_ = '1522028472133';
export const healthShareUrlBase = 'https://api.healthshare.com.au';

export const specialtiesUrl               = healthShareUrlBase + '/professionals/v1/specialties/';
export const practitionersUrl             = healthShareUrlBase + '/professionals/v1/search/';
export const practitionersBySpecAndLocUrl = healthShareUrlBase + '/professionals/v1/search/location/';
export const practitionerUrl              = healthShareUrlBase + '/professionals/v1/';

@Injectable({
  providedIn: 'root'
})
export class HealthshareService {

  public specialtiesAll: Specialty[];

  constructor(
    private http: HttpClient
  ) {
    // get specialities
    this.getSpecialties()
      .subscribe((data: Specialty[]) => this.specialtiesAll = data);

  }

  private getSpecialties(): Observable<Specialty[]> {
    return this.http
      .get<Specialty[]>(specialtiesUrl)
      .pipe(
        catchError(this.handleError('getSpecialties', []))
      );
  }

  getPractitioner(id: number): Observable<PractitionerDetail> {
    const params = new HttpParams({ fromObject:
        { show: 'referrals',
          Token: apiToken,
          _: apiToken_ }
    } );
    const headers = new HttpHeaders({}) ;
    const options = { headers , params };

    return this.http.get<PractitionerDetail>(practitionerUrl + id + '/', options)
      .pipe(
        catchError(this.handleError('getPractitioner', null))
    );
  }

  searchPractitionersByName(searchString, refresh = false): Observable<PractitionerSearchResult> {
    // clear if no practitioner name or if only one character typed
    if (searchString.trim().length < 2) { return of(null); }

    const params = new HttpParams({ fromObject:
        { name: searchString,
          limit: '20',
          show: 'referrals',
          Token: apiToken,
          _: apiToken_ }
    } );
    const headers = new HttpHeaders({}) ;
    const options = { headers , params };

    // TODO: Add error handling
    return this.http.get<PractitionerSearchResult>(practitionersUrl, options)
      .pipe(
        catchError(this.handleError('searchPractitionersByName', null))
      );

  }

  searchPractitionersBySpecialtyAndLocation(specialtyId: string, locality: string): Observable<PractitionerSearchResult> {
    const params = new HttpParams({ fromObject:
        { specialty: specialtyId,
          locality: locality,
          limit: '20',
          page: '1',
          show: 'referrals',
          Token: apiToken,
          _: apiToken_ }
      } );

    const headers = new HttpHeaders({}) ;
    const options = { headers , params };

    // TODO: Handle errors eg 'invalid specialty' error
    return this.http.get<PractitionerSearchResult>(practitionersBySpecAndLocUrl, options)
      .pipe(
        catchError(this.handleError('searchPractitionersBySpecialtyAndLocation', null))
    );

  }

  searchSpecialtiesByName(searchString): Observable<any[]> {
    // clear if no name or if only one character typed
    if (searchString.trim().length < 2) { return of([]); }

    return of(this.specialtiesAll.filter(
      specialty => {
        return (this.matchSpecialtyName(<Specialty>specialty, searchString));
      }
    ));
  }

  private matchSpecialtyName(specialty: Specialty, searchString: string): boolean {
    const specialityNamesConcat = [
      specialty.name,
      specialty.plural_name,
      specialty.professional_name,
      specialty.plural_professional_name].join(' ');
    return (specialityNamesConcat.toLowerCase().indexOf( searchString.trim().toLowerCase() ) > -1);
  }

  getSpecialtyNames( specialtyIds: number[]): string[] {
    const specialtyNames = this.specialtiesAll
      .filter( specialty =>
        specialtyIds.includes(specialty.id)
      )
      .map(specialty => specialty.name );
    return specialtyNames;
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`HealthshareService: ${message}`);
  }
}

