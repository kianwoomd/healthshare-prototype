import { Practice } from './models/practice.interface';

export enum searchSteps {
  Suggested = 1,
  Matching,
  PractitionersBySpecialtyId,
  PractitionerDetail,
}

export interface State {
  searchStep: searchSteps;
  searchStepPrevious: searchSteps;
  selectedSpecialtyId: number;
  selectedPractitionerId: number;
  selectedTabIndex: number;
}
