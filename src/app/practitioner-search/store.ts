// Custom simple store implementation
import { searchSteps, State } from './state';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, pluck } from 'rxjs/operators';

let state: State = {
  searchStep: searchSteps.Suggested,
  searchStepPrevious: searchSteps.Suggested,
  selectedSpecialtyId: undefined,
  selectedPractitionerId: undefined,
  selectedTabIndex: 1 // dont use 0
};

export class Store {

  private subject = new BehaviorSubject<State>(state);
  private store = this.subject.asObservable()
    .pipe(
      distinctUntilChanged()
    );

  get value() {
    return this.subject.value;
  }

  select<T>(name: string): Observable<T> {
    return this.store
      .pipe(
        pluck(name)
      );
  }

  set(name: string, statex: any) {
    this.subject.next({
      ...this.value, [name]: statex
    });
  }

}
